# Linux.Community

Linux.Chat Communities was launched on 16 June 2023 as a [Lemmy](https://join-lemmy.org/) instance for people who are passionate about, support, or work with Linux®. Our community discussions aren't limited to just Linux®-related or opensource-related topics, but we do require a reasonable ratio of content to be on-topic.

Posting in English is recommended for maximum conversation opportunities within the community. You will need to understand English in order to agree to our Community Code of Conduct.

This service was installed and is maintained by [Linux.Chat](https://linux.chat) with equipment, hosting and object storage sponsored by [Conarx, Ltd](https://conarx.tech) located at [OVH](https://www.ovh.com).

Please contact [@admin@Linux.Community](https://Linux.Community/@admin) or email us on [admin@Linux.Community](mailto:admin@Linux.Community) with any moderation or administrative concerns. Additional information about reporting concerns related to the Community Code of Conduct can be found below.

For media and press-related topics please use [contact@Linux.Community](mailto:contact@Linux.Community).


## How to Thrive on Linux.Community

- Browse the Local Communities and get to know others in our community.
- Add respectful replies and conversation to others' posts you find interesting.
- Report content that you find concerning, whether it's from Linux.Community people or other servers.
- We like politics and religion, however there are instances more suited to these topics.
- We're all here because we want a fun, friendly, safe and enjoyable place to communicate, share and collaborate free of the toxicity encountered on other platforms on topics related to Linux®.
- We encourage everyone to steer discussions in a productive direction. For more details, review our [Catalyst Guidelines](https://linux.chat/catalyst-guidelines/)



# Linux.Community Code of Conduct


## Our Pledge to Each Other

In the interest of fostering an open, positive, fun, friendly and welcoming environment, we as Linux.Community members, moderators, and admins (collectively known as Linux.Community members) pledge to make our participation in the Linux.Chat group of communities and the greater Fediverse a pleasant and harassment-free experience for everyone.


## Diversity is Encouraged

Linux.Chat aims to provide a positive, safe environment not only everyone in its communities but also those who interact with us, regardless of:

- age;
- body size;
- caste;
- disability;
- ethnicity;
- gender identity and expression;
- level of experience;
- education;
- socio-economic status;
- nationality;
- personal appearance;
- race;
- religion (or lack thereof);
- sexual identity and orientation;
- tribe; or
- other protected status as defined by laws or the [Universal Declaration of Human Rights](https://www.un.org/en/about-us/universal-declaration-of-human-rights)


## Our Standards

Examples of behavior that contributes to creating an open, positive, fun, friendly and welcoming environment include, but are not limited to:

- Using welcoming, inclusive and friendly language.
- Being respectful of differing viewpoints and experiences.
- Gracefully accepting constructive criticism and respectively giving it.
- Focusing on what is best for the community.
- Showing empathy towards other community members.

Examples of unacceptable behavior by Linux.Chat community members include, but are not limited to:

- Insulting/derogatory comments.
- Trolling.
- Statements or jokes that exclude based on the demographic groups in the above "Diversity is Encouraged" section.
- Deliberate intimidation, stalking, or following.
- Public or private harassment.
- Publishing others’ private information, such as a physical or electronic address, without explicit permission.
- Sexual language, imagery, attention, or advances towards others, or behavior that contributes to a sexualized environment.
- Violence directed against another person, including violent threats and violent language.
- Expressing support for such unacceptable behaviors by others.

Linux.Community prioritizes marginalized people's safety over privileged people's comfort. Linux.Community moderators and admins will not take action on complaints regarding:

- "Reverse"-isms, including "reverse racism," "reverse sexism," and "cisphobia".
- The tone or writing style used when responding to harassment, racism, sexism, transphobia, or otherwise oppressive behavior or assumptions.
- Reasonable communication of boundaries, such as "leave me alone," "go away," or "I'm not discussing this with you".


## Our Responsibilities

Linux.Community moderators and admins are responsible for clarifying the standards of acceptable behavior and are expected to take appropriate and fair corrective action in response to any instances of unacceptable behavior.

Linux.Community moderators and admins have the right and responsibility to remove, edit, or reject posts, messages, comments, commits, code, or any other contributions that are not aligned to this Code of Conduct.

Linux.Community moderators and admins have the right to take certain action in response to any Linux.Community member exhibiting other behaviors that they deem inappropriate, threatening, offensive, harmful or causes damage to the reputation of the Linux.Community or Linux.Chat group of communities.

Examples of such actions include, but are not limited to:

- Removal or limiting viewability of content posted to Linux.Community.
- A temporary, permanent or global ban from all the Linux.Chat group of communities.
- A temporary, permanent or global ban from applying for a Linux.Community account in the future or global ban across the Linux.Chat group of communities.
- A temporary, permanent or global ban from participating as a Linux.Community moderator or admin or global ban across the Linux.Chat group of communities.


## Scope

This Code of Conduct applies to all Linux.Chat community spaces, including Linux.Community and Linux.social, herein collectively referred to as Linux.Chat:

- The Linux.Chat and Linux.Chat-related websites.
- The Linux.Chat public and private mailing lists.
- Any official public or private Linux.Chat forums or rooms on Matrix, IRC and Discord.
- Any Linux.Chat public and private git repositories.
- Any additional Linux.Chat project spaces or communities added in the future.

When Linux.Chat community members contribute in discussion in or from our services, they represent the Linux.Chat community and its reputation.

Therefore, this Code of Conduct applies to:

- Public communication between Linux.Chat community members in community spaces of Linux®-related, free software projects or non-profits who are also Linux®-related community members.
- Public communication between Linux.Chat community members at gatherings related to Linux.Chat and at Linux®-related events generally.
- Public Linux.Chat social media posts and comments by Linux.Chat community members on those social media posts.
- Private communication between Linux.Chat community members related to Linux.Chat communities or its members.
- Private communication between Linux.Chat community members and Linux.Chat admins or moderators.

This Code of Conduct also applies to official Linux.Chat communication, such as:

- Using an official Linux.Chat e-mail address.
- Acting as an appointed representative at an online or offline event.
- Posting via an official Linux.Chat social media account.

Representation of our community may be further defined and clarified by Linux.Chat admins and moderators.

If a concern about a past incident in any space is made known to a moderator or admin about a current Linux.Chat community member, Linux.Chat moderators or admins may take it into account when deciding whether the incident should have any consequences to the community member's participation in the Linux.Chat community.


## Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be reported by:

- Using the "Report" menu item on any post or profile in the Lemmy software (preferred).
- Contacting Linux.Chat admins directly through the @admin@Linux.Community account.
- Contacting Linux.Chat admins or moderator privately through their official Linux.Chat community account.
- Sending an email to [abuse@Linux.Chat](mailto:abuse@linux.chat).

All complaints will be reviewed and investigated as quickly as possible, and will result in a response that is deemed necessary and appropriate to the circumstances. The Linux.Chat admins and moderators are obligated to maintain confidentiality with regard to the reporter of an incident. Further details of specific enforcement policies may be posted separately.


## Attribution

This Code of Conduct is adapted from the [FLOSS.social Code of Conduct](https://floss.social/about) as of December 21, 2022 and the [Outreachy Community Code of Conduct](https://github.com/outreachy/website/blob/478c85f084de058ad8e4249d3c8d15d4025ed66d/CODE-OF-CONDUCT.md) as of January 28, 2002, made available under the terms of the [GNU General Public License, version 3](https://github.com/outreachy/website/blob/478c85f084de058ad8e4249d3c8d15d4025ed66d/LICENSE.md), and this "Linux.Chat Community Code of Conduct" is published under those same terms. The Outreachy Community Code of Conduct was adapted from the [Contributor Covenant, version 1.4](https://www.contributor-covenant.org/version/1/4/code-of-conduct.html).


## DMCA Takedown Notices

DMCA takedown notices may be submitted to [dmca@Linux.Community](mailto:dmca@Linux.Community).

Please ensure your DMCA takedown notice meets the following requirements:

- The signature of the copyright owner or owner’s agent, in electronic form.
- Identification of the: (i) copyrighted work(s) infringed; (ii) the infringing activity; and (iii) the location of the infringing activity (by providing the URL).
- Contact information of the notice sender, including an email address.
- A statement that the notifier has a good faith belief that the material is not authorized by the intellectual property or copyright owner, its agent, or the law.
- A statement that the information provided is accurate and the notifier is authorized to make the complaint on behalf of the intellectual property or copyright owner.

Please also take note of the DMCA takedown procedure:

- A copyright holder files a takedown notice, under penalty of perjury, with Linux.Community claiming that the site is hosting infringing content owned by the copyright holder;
- Linux.Community will provide the posting party (24) hours to remove the content, should they fail to action this, we will remove the content within (24) hours;
- The posting party then has the right to file a counter-notification, informing Linux.Community that the content is not infringing; and
- If a counter-notice is filed, the service provider must re-host the content unless the original copyright holder files a lawsuit.


# Linux.Community Inactive Account Policy

In an effort to free up a wide range of account names, we may delete inactive accounts and associated personal data.

An account is considered inactive when:

- no posts have been made on the account and it has been at least twelve (12) months since the account was last accessed; or
- it has been at least twenty-four (24) months since the account was last accessed

For purposes of this policy, "accessed" means you have interacted with your account through one or more signed-in session from any device.

In order to warn the users concerned, an email will be sent to the e-mail address on file for the account providing the user twenty-one (28) days to reply and request an appeal for the account to be restored. Once an account has been deleted and the warning period has passed, it can no longer be recovered. To use Linux.Community again, you'll need to create a new account.


# Server Policies and Information

Our server policies and information can be contributed to via our [GitLab instance repository](https://gitlab.linux.community/linux.chat/linux.lommunity/server-information).
