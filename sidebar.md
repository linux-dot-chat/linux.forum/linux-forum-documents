### Other Communities

Linux.Chat community info can be found [here](https://linux.chat).

- [@linux.social Mastodon](https://linux.social)
- [#linux on Libera.Chat](https://linux.chat/linux-on-libera/how-to-connect/)
- [Linux.Chat on Discord](https://discord.gg/linuxchat)
- [Linux.Chat Support Forums](https://linux.forum)


### Financial Sustainability

You can support our community financially through the [Linux.Chat OpenCollective](https://opencollective.com/linuxchat), with
options as low as £1 per month.
