# Rules

- Adhere to our Community Code of Conduct.
- Use welcoming, inclusive and friendly language.
- Be respectful of differing viewpoints and experiences.
- Gracefully accept constructive criticism and give it - where needed - only respectfully.
- Show empathy towards other community members and focus on what is best for the community.
- No sexualized language, imagery, or sexual attention/advances.
- No trolling, insulting/derogatory comments, or personal/political/religious attacks.
- No public or private harassment.
- Do not publish others' private information, such as a physical or electronic addresses, without explicit permission.
- Do not engage in conduct which could reasonably be considered inappropriate in a professional setting.
- No posting of commercial or business content (unless sharing your Linux®-related activities or experiences).
- No posting or boosting the same content repeatedly or frequently.
- No botting or cross-posting from other services if those posts include jargon or links to those services.
- Avoid political and religious topics, there are instances better suited to these topics.
- A reasonable ratio of posts must contain Linux®-related or opensource-related content.
- Refer to our Privacy Policy regarding minimum age requirements for using this server.


# New Accounts

All new accounts require an application to be submitted with a reason/motivation for requesting the account. Single word or low
quality reasons/motivation will be rejected.

Examples of low quality reasons and motivations:
- "yes"
- "because i want one"
- "testing"
- "i'm curious"

Examples of high quality reasons and motivations:
- "I'm a Linux® user for the last 1 year and I'm looking for a community where I can learn and make friends"
- "I wish to create a community myopensourcesoftware"
- "I'm a member of #linux on Libera.Chat with nickname examplecom"
- "I'm a Linux® sysadmin and want to register an account and join the community"

These are just examples, as long as your reason/motivation is reasonable it should be accepted.

If one disagrees with this requirement, there are many Lemmy instances which have open registration and your ability to browse our
content and interact with our community is not hindered.


# Communities

We encourage the forming of new communities, however please remember this instance is Linux® and opensource software themed.

- Communities should be Linux® or opensource software related.
- Communities are required to have at least 2 active moderators.
- Community moderators are required to adhere to and enforce our Code of Conduct.

Unmoderated communities or communities with inactive moderators may be disabled/restricted/removed or re-assigned.
